#!/bin/bash

export TZ="America/New_York"

export LD_PRELOAD="./libfaketime.so.1"
export FAKETIME_DONT_RESET=1
export FAKETIME_DONT_FAKE_MONOTONIC=1


# March DST 2023
export FAKETIME="@2023-03-11 19:02:00"
javac -cp "./jtds-1.3.1.jar" Main.java && java -cp "./jtds-1.3.1.jar:./" Main

export FAKETIME="@2023-03-11 20:02:00"
java -cp "./jtds-1.3.1.jar:./" Main

export FAKETIME="@2023-03-11 21:02:00"
java -cp "./jtds-1.3.1.jar:./" Main

export FAKETIME="@2023-03-11 22:02:00"
java -cp "./jtds-1.3.1.jar:./" Main

export FAKETIME="@2023-03-11 23:02:00"
java -cp "./jtds-1.3.1.jar:./" Main

export FAKETIME="@2023-03-12 0:02:00"
java -cp "./jtds-1.3.1.jar:./" Main

export FAKETIME="@2023-03-12 1:02:00"
java -cp "./jtds-1.3.1.jar:./" Main

export FAKETIME="@2023-03-12 3:02:00"
java -cp "./jtds-1.3.1.jar:./" Main

export FAKETIME="@2023-03-12 4:02:00"
java -cp "./jtds-1.3.1.jar:./" Main

