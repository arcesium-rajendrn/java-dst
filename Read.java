import java.util.*;
import java.text.*;
import java.sql.Timestamp;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class Read {
	public static void main(String[] $) throws Exception, ClassNotFoundException {
    // DB CODE
    // Connection part
    Thread.sleep(2000);
    Class.forName("net.sourceforge.jtds.jdbc.Driver");
    String dbURL = "jdbc:jtds:sqlserver://localhost:1433/master";

    Connection conn = DriverManager.getConnection(dbURL, "sa", "Pass1234");
    if (conn != null) {
        System.out.println("Connected");
    }

    String sql = "select * from datetest";

    // Set parameters
    Statement st = conn.createStatement();
    ResultSet rs = st.executeQuery(sql);

    while (rs.next()) {
      Date[] date = new Date[8];
      int row = rs.getInt(1);
      System.out.println(row);

      for (int i = 2; i < 8; ++i) {
      String concat = "";
        date[i] = rs.getTimestamp(i);
        concat += "Date.toString() = " + date[i].toString() + "; Date.getTime() =  " + date[i].getTime();
        System.out.println(concat);
      }

      System.out.println("-----------");

    }
    System.out.println("Done");
	}

}
