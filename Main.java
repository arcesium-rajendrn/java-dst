import java.util.*;
import java.text.*;
import java.sql.Timestamp;
import java.sql.PreparedStatement;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class Main {
	public static TimeZone UTCTZ = TimeZone.getTimeZone("UTC");
	public static TimeZone UTC_TIMEZONE = TimeZone.getTimeZone("UTC");
  public static String yyyyMMddSpaceHHmmssSSSColonFormat = "yyyyMMdd HH:mm:ss.SSS";

  // Global new date
  public static Date newDate = new Date();

	public static void main(String[] $) throws Exception, ClassNotFoundException {
    // DB CODE
    // Connection part
    Thread.sleep(2000);
    Class.forName("net.sourceforge.jtds.jdbc.Driver");
    String dbURL = "jdbc:jtds:sqlserver://localhost:1433/master";
    // String dbURL = "jdbc:jtds:sqlserver://rajendrn-dev-aps1.workspaces.corp.win.ia55.net:1433/master";
    Connection conn = DriverManager.getConnection(dbURL, "sa", "Pass1234");
    if (conn != null) {
        System.out.println("Connected");
    }

    newDate = new Date();

    System.out.println(formatDateWithUTCTimeZone(newDate, false));
    System.out.println("Date.getTime(): " + newDate.getTime());
    String sql = "insert into datetest values (?, ?, ?, ?, ?, ?, ?);";
    Double sqlEpochDays = getSqlEpochDays(newDate);

    LocalDateTime localDateTime = newDate.toInstant().atZone(ZoneId.of("UTC")).toLocalDateTime();
    System.out.println("LocalDate: " + localDateTime.toString());

    // Set parameters
    System.out.println("Epoch days: " + sqlEpochDays);
    PreparedStatement st = conn.prepareStatement(sql);
    st.setInt(1, 2);
    st.setTimestamp(2, new Timestamp(newDate.getTime()));
    st.setTimestamp(3, new Timestamp(getCurrentUTCDate().getTime()));
    st.setTimestamp(4, new Timestamp(getNew().getTime()));
    st.setTimestamp(5,
            new Timestamp(newDate.getTime()), Calendar.getInstance(TimeZone.getTimeZone("UTC")));
    // set the epoch in days
    st.setDouble(6, sqlEpochDays);
    st.setString(7, localDateTime.toString());

    st.execute();
    System.out.println(st);
    System.out.println("Done");
	}

  public static Double getSqlEpochDays(Date date) {
    long dayMillis = 24 * 60 * 60 * 1000;

    long curr = date.getTime();
    long offset = 25567; // Number of days since 19000101

    return 1.0d * curr / dayMillis + offset;

  }

  public static String formatDateWithUTCTimeZone(Date date, boolean legacy) {
    DateFormat dateFormat = new SimpleDateFormat(yyyyMMddSpaceHHmmssSSSColonFormat);
    if (!legacy) {
        date = convertTimeZone(date,  Calendar.getInstance().getTimeZone(), UTC_TIMEZONE);
    }
    return dateFormat.format(date);
  }

	public static Date convertTimeZone(Date inputDate,
                                       TimeZone currentTimeZone,
                                       TimeZone destTimeZone) {
		Date resultDate = null;

		if (inputDate       != null &&
		    currentTimeZone != null &&
		    destTimeZone    != null)
		{
      long currentOffset = currentTimeZone.getOffset(inputDate.getTime());
      long destOffset = destTimeZone.getOffset(inputDate.getTime());
      long diffOffset = (destOffset - currentOffset);
      // System.out.println("Offset : " + diffOffset);
      resultDate = new Date(inputDate.getTime() + diffOffset);
		}

		return resultDate;
	}

	public static Date getCurrentUTCDate() {
		Calendar beginCalendar = Calendar.getInstance();

		Date date = convertTimeZone(beginCalendar.getTime(),
			beginCalendar.getTimeZone(), UTC_TIMEZONE);

		return date;
	}

  public static Date getNew() {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");

    Date date = null;
    try {
      date = localDateFormat.parse(simpleDateFormat.format(newDate));
    } catch (Exception e) {}

    return date;
  }

}
