## SQL Server setup
Start a local SQL Server
```bash
sudo docker run -e "ACCEPT_EULA=Y" -e "MSSQL_SA_PASSWORD=Pass1234" -p 1433:1433 --name sql1 --hostname sql1 -d mcr.microsoft.com/mssql/server:2022-latest
```
Now you can connect to this server using below username and password in SSMS/DBeaver.

username=sa
password=Pass1234

Create the table
```sql
create table datetest (rv int,
newDate datetime,
getCurrentUTCDate datetime,
newmethod datetime,
useCalendar datetime,
sendTheDateAsDouble datetime,
sendTheDateAsString datetime)
```

### Writer

From the repo, run the below command (Give execute permission on the script - chmod 700 simulate_dst.sh)
```bash
./simulate_dst.sh
```

Check the database to verify if the date is correctly getting persisted.
```sql
select *
from datetest
```

### Reader
The mybatis type handler is not able to interpret 2AM - 3AM in UTC and convert it correctly to java Date. 2AM is getting converted to 3AM. This is because the jtds.DateTime is using java.util.GregorianCalendar to convert jtds.DateTime to java.sql.Timestamp which extends java.util.Date.

```bash
./read_dst.sh
```

Execute greg.sh to see how java.util.GregorianCalendar behaves during the DST time shift. The GregorianCalendar is used by net.sourceforge.jtds.jdbc.DateTime class to convert sql date time to java.sql.Timestamp. 
```bash
./greg.sh
```
