import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.Date;
import java.sql.Timestamp;

public class Greg {
  public static void main(String[] $) {

    int year = 2023;
    int month = 3;
    int day = 12;
    int hour_start = 1;
    int minute = 51;
    int second = 27;
    int millis = 0;

    for (int hour = hour_start; hour <= hour_start + 4; ++hour) {
      GregorianCalendar cal = new GregorianCalendar();
      cal.set(Calendar.YEAR, year);
      cal.set(Calendar.MONTH, month - 1);
      cal.set(Calendar.DAY_OF_MONTH, day);
      cal.set(Calendar.HOUR_OF_DAY, hour);
      cal.set(Calendar.MINUTE, minute);
      cal.set(Calendar.SECOND, second);
      cal.set(Calendar.MILLISECOND, millis);
      Timestamp tsValue = new Timestamp(cal.getTime().getTime());
      System.out.println("Hour = " + hour + "; " + tsValue.toString() + " : " + tsValue.getTime());
    }

  }
}
